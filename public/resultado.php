<?php
include_once '../src/Controladores/Inscripcion.php';

use Controladores\Inscripcion as Controlador;

$controlador = new Controlador();
$error = array_key_exists('error', $_SESSION) ? $_SESSION['error'] : null;

if (array_key_exists('email', $_SESSION) && !$error) {
    $controlador->mostrarExito();
} elseif (!$error) {
    $controlador->mostrarError('Ha ocurrido un error inesperado');
} else {
    $controlador->mostrarError($error);
}
