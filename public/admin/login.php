<?php
include_once '../../src/Controladores/Login.php';

use Controladores\Login as Controlador;

$controlador = new Controlador();

if (array_key_exists('admin_id', $_SESSION)) {
    header("Location: /admin/index.php");
    die();
}

$error = array_key_exists('error', $_SESSION) ? $_SESSION['error'] : null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $controlador->authenticar($_POST);
} else {
    $controlador->mostrarLogin($error);
    unset($_SESSION['error']);
}
