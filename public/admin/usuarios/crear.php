<?php
include_once '../../../src/Controladores/Usuario.php';

use Controladores\Usuario as Controlador;

$controlador = new Controlador();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $controlador->guardar($_POST);
} else {
    $controlador->crear();
}
