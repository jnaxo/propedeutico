<?php
include_once '../../../src/Controladores/Taller.php';

use Controladores\Taller as Controlador;

$controlador = new Controlador();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $controlador->guardar($_POST);
} else {
    $controlador->crear();
}
