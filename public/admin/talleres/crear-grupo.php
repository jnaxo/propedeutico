<?php
include_once '../../../src/Controladores/Taller.php';

use Controladores\Taller as Controlador;

$controlador = new Controlador();
$taller_id = $_GET['taller'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $controlador->guardarGrupo($taller_id, $_POST);
} else {
    $controlador->crearGrupo($taller_id);
}
