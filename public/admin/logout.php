<?php
session_start();

if (array_key_exists('admin_id', $_SESSION)) {
    unset($_SESSION['admin_id']);
}
header("Location: /admin/login.php");
die();
