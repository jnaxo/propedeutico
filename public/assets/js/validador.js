function validate(step, workshops) {
  switch (step) {
    case 1:
      return validateData();
    case 2:
      return validateWorkshop(workshops);

    default:
      return false;
  }
}

function validateData() {
  var hasError = false;
  // RUT
  if (!validateRut($('#rut').val())) {
    $('#rut').addClass('is-invalid');
    hasError = true;
  } else {
    $('#rut').removeClass('is-invalid');
  }
  // names
  if (!validateName($('#name').val())) {
    $('#name').addClass('is-invalid');
    hasError = true;
  } else {
    $('#name').removeClass('is-invalid');
  }

  if (!validateName($('#firstSurname').val())) {
    $('#firstSurname').addClass('is-invalid');
    hasError = true;
  } else {
    $('#firstSurname').removeClass('is-invalid');
  }

  if (!validateName($('#secondSurname').val())) {
    $('#secondSurname').addClass('is-invalid');
    hasError = true;
  } else {
    $('#secondSurname').removeClass('is-invalid');
  }

  // phone
  if (!validatePhone($('#phone').val())) {
    $('#phone').addClass('is-invalid');
    hasError = true;
  } else {
    $('#phone').removeClass('is-invalid');
  }

  // email
  if (!validateEmail($('#email').val())) {
    $('#email').addClass('is-invalid');
    hasError = true;
  } else {
    $('#email').removeClass('is-invalid');
  }

  // selects
  if ($('#school').val() === '') {
    $('#school').addClass('is-invalid');
    hasError = true;
  } else {
    $('#school').removeClass('is-invalid');
  }

  if ($('#course').val() === '') {
    $('#course').addClass('is-invalid');
    hasError = true;
  } else {
    $('#course').removeClass('is-invalid');
  }

  $('#crut').text($('#rut').val());
  $('#cname').text($('#name').val());
  $('#cname').text($('#name').val() + ' ' + $('#firstSurname').val() + ' ' + $('#secondSurname').val().substring(0,1) + '.');
  $('#cphone').text($('#phone').val());
  $('#cemail').text($('#email').val());
  $('#cschool').text($('#school option:selected').text());
  $('#ccourse').text($('#course option:selected').text());

  return !hasError;
}

function validateWorkshop(workshops) {
  $('#workshop').removeClass('is-invalid');
  $('#workshopGroup').removeClass('is-invalid');
  if (workshops.length < 1) {
    if (!$('#workshop').val()) {
      $('#workshop').addClass('is-invalid');
    }
    $('#workshopGroup').addClass('is-invalid');
    return false;
  }
  return true;
}

function validateRut(rut) {
  if (typeof rut !== 'string') {
    return false;
  }

  if (!/^0*(\d{1,3}(\.?\d{3})*)-?([\dkK])$/.test(rut)) {
    return false;
  }

  if (rut.length < 9) {
    return false;
  }

  const cleanRut = rut.replace(/^0+|[^0-9kK]+/g, '').toUpperCase();
  let t = parseInt(cleanRut.slice(0, -1), 10);
  let m = 0;
  let s = 1;

  while (t > 0) {
    s = (s + (t % 10) * (9 - (m % 6))) % 11;
    t = Math.floor(t / 10);
    m += 1;
  }

  const v = s > 0 ? `${(s - 1)}` : 'K';
  return v === cleanRut.slice(-1);
};

function validateName(name) {
  return /^([^0-9]*)[a-zA-Z ]+([^0-9]*)$/.test(name);
}

function validatePhone(phone) {
  number = phone.replace('+', '').replace(/\s/g, '').replace(/()/g, '');
  var isValid = true;
  if (!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(number)) {
    isValid = false
  }

  if (number.length < 8 || number.length > 11) {
    isValid = false
  }
  return isValid;
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
