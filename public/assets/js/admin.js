$(function() {
  $('.count').each(function () {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function (now) {
          $(this).text(Math.ceil(now));
      }
    });
  });

  // Grafico inscritos
  var datos = [
    { label: '1º Medio' },
    { label: '2º Medio' },
    { label: '3º Medio' },
    { label: '4º Medio' }
  ];

  var colores = {
    '1º Medio': "rgb(54, 162, 235)",
    '2º Medio': "rgb(75, 192, 192)",
    '3º Medio': "rgb(255, 159, 64)",
    '4º Medio': "rgb(153, 102, 255)",
  }

  datos.map(function (e) {
    var arreglo = [];
    talleres.map(function (e) {
      arreglo.push(0);
    });
    e.data = arreglo;
    cursosTaller.filter(function(el) {
      return el['Usuario.Curso'] === e.label;
    }).map(function (el) {
      e.data[talleres.indexOf(el['Taller.Nombre'])] = parseInt(el.cantidad);
    });
    e.backgroundColor = colores[e.label];
  });

  var ctx = document.getElementById('inscritosCurso').getContext('2d');
  var chart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: talleres,
        datasets: datos
      },

      options: {
        title: {
          display: true,
          text: 'Inscritos por taller clasificados por año cursado'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: true,
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              stepSize: 1
           }
          }]
        }
      }
  });

  // grafico colegios
  var todosColores = {
    blue: "rgb(54, 162, 235)",
    green: "rgb(75, 192, 192)",
    orange: "rgb(255, 159, 64)",
    purple: "rgb(153, 102, 255)",
    red: "rgb(255, 99, 132)",
    yellow: "rgb(255, 205, 86)",
    grey: "rgb(201, 203, 207)",
  };

  var ctx2 = document.getElementById('alumnosColegios').getContext('2d');
  var chart2 = new Chart(ctx2, {
    type: 'doughnut',
    data: {
      datasets: [{
          data: colegios.map(function(el) {
            return el.cantidad
          }),
          backgroundColor: colegios.map(function(el,i) {
            return Object.values(todosColores)[i%7];
          }),
      }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: colegios.map(function(el) {
      return el['Colegio.Nombre']
    })
    },
    options: {
      title: {
        display: true,
        text: 'Inscritos por Colegio'
      },
      tooltips: {
        mode: 'index',
        intersect: false
      },
      maintainAspectRatio : false,
      responsive: true
    }
  });
});
