var step = 1;
var sequence = 1;
var workshopsSelected = [];
var horariesSelected = [];

function displayStep(prev, next) {
  $('#step-' + prev).hide()
  $('#step-' + next).show()
  btnStep();
}

function btnStep() {
  $('#btn-' + step).addClass('btn-primary')
  $('#btn-' + step).removeClass('btn-success')
  $('#btn-' + step).removeClass('btn-secondary')

  if (step < 3) {
    $('#btn-' + (step + 1)).addClass('btn-secondary')
    $('#btn-' + (step + 1)).removeClass('btn-success')
    $('#btn-' + (step + 1)).removeClass('btn-primary')
  }

  if (step > 1) {
    $('#btn-' + (step - 1)).addClass('btn-success')
    $('#btn-' + (step - 1)).removeClass('btn-secondary')
    $('#btn-' + (step - 1)).removeClass('btn-primary')
  }
}

function removeBtn (id) {
  return $('<button type="button" id="' + id + '" class="btn btn-outline-danger btn-sm rm-workshop">X</button>');
}

function addWorkshop(workshop, horary) {
  workshopsSelected.push(workshop);
  horariesSelected.push(horary);

  refreshSubscription();

  $('#workshop').val('');
  $('#workshopGroup').val('');
  $('#workshopGroup').attr('disabled', true);
  $('#workshop').removeClass('is-invalid');
  $('#workshopGroup').removeClass('is-invalid');
  reloadWorkshops();

  var groups = [];
  for (var i = 0; i < horariesSelected.length; i++) {
    groups.push(horariesSelected[i].Id);
  }
  $('#groupsInput').val(JSON.stringify(groups));
}

function removeWorkshop() {
  var rmId = parseInt($(this).attr('id'));
  for (var i = 0; i < workshopsSelected.length; i++ ) {
    if (workshopsSelected[i].Id === rmId) {
      workshopsSelected.splice(i, 1);
      horariesSelected.splice(i, 1);
      break;
    }
  }
  refreshSubscription();
  reloadWorkshops();
}

function reloadWorkshops() {
  $('#workshop option').each(function() {
    if ($(this).val()) {
      $(this).remove();
    }
  });

  for (var i = 0; i < workshops.length; i++) {
    if (!workshopsSelected.includes(workshops[i])) {
      $('#workshop').append(new Option(workshops[i].Nombre, workshops[i].Id));
    }
  }
}

function refreshSubscription() {
  $('#workshop-table tbody').html("");
  $('#display-workshop-table tbody').html("");

  for (var i = 0; i < workshopsSelected.length; i++) {
    var rmBtn = removeBtn(workshopsSelected[i].Id);
    rmBtn.bind('click', removeWorkshop);
    $('#workshop-table tbody')
      .append('<tr><th scope="row">'
        + workshopsSelected[i].Id
        + '</th><td>'
        + workshopsSelected[i].Nombre
        + '</td><td>'
        + horariesSelected[i].Horario
        + '</td><td></td></tr>');
    $('#workshop-table tbody>tr>td:last').append(rmBtn);
  }
  for (var i = 0; i < workshopsSelected.length; i++) {
    $('#display-workshop-table tbody')
      .append('<tr><th scope="row">'
        + workshopsSelected[i].Id
        + '</th><td>'
        + workshopsSelected[i].Nombre
        + '</td><td>'
        + horariesSelected[i].Horario
        + '</td></tr>');
  }

  if (workshopsSelected.length < 1) {
    $('#workshops-preview').hide();
  }
}

$(function() {
  // load data
  reloadWorkshops();
  for (var i = 0; i < schools.length; i++) {
    $('#school').append(new Option(schools[i].Nombre, schools[i].Id));
  }

  // back button
  $('.back-btn').click(function () {
    if (step > 1) {
      step -= 1;
      if (step == 1) {
        $('.back-btn').attr('disabled', true);
      } else {
        $('.back-btn').attr('disabled', false);
      }
    }
    $('.next-btn').attr('disabled', false);
    displayStep(step + 1, step);
  })

  // continue button
  $('.next-btn').click(function (e) {
    if (!validate(step, workshopsSelected)) {
      return;
    }

    if (step < 3) {
      step += 1;
    }
    $('.back-btn').attr('disabled', false);
    displayStep(step - 1, step);
  })

  $('#workshop').change(function () {
    $(this).removeClass('is-invalid');
    $('#workshopGroup').val('');
    $('#workshopGroup option').each(function() {
      if ($(this).val()) {
        $(this).remove();
      }
    });

    var id = $( "#workshop").val();
    var workshop = workshops.find(function (el) {
      return el.Id == id;
    });

    if (id) {
      $('#workshopGroup').attr('disabled', false);
      for (var i = 0; i < workshop.Grupos.length; i++) {
        $("#workshopGroup").append(new Option(workshop.Grupos[i].Horario, workshop.Grupos[i].Id));
      }
    } else {
      $('#workshopGroup').attr('disabled', true);
    }
  });

  $('#workshopGroup').change(function () {
    var id = $( "#workshop").val();
    var groupId = $( "#workshopGroup").val();
    var workshop = workshops.find(function (el) {
      return el.Id == id;
    });

    if (groupId) {
      $('#workshops-preview').show();
      addWorkshop(workshop, workshop.Grupos.find(function (el) {
        return el.Id == groupId
      }));
    }
  })
});
