<?php
include_once '../src/Controladores/Inscripcion.php';

use Controladores\Inscripcion as Controlador;

$controlador = new Controlador();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $controlador->inscribir($_POST);
} else {
    $controlador->index();
}
