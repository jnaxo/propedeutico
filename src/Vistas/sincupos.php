<!doctype html>
<html lang="en">
  <head>
    <?php require_once '../src/Vistas/parciales/cabecera.php' ?>

    <title>Inscripción Propedeutico</title>
    <link rel="stylesheet" type="text/css" href="assets/css/app.css">
  </head>
  <body>
    <main class="container">

      <div class="row mt-5 justify-content-md-center">
        <div class="col-sm-12 col-md-8 col-xl-6">
          <h1 class="text-center mb-5">Inscripción Propedeutico</h1>

          <div class="shadow-lg p-3 mb-3 mt-3 bg-white" id="step-3">
            <div class="d-flex justify-content-center m-4">
              <div class="col-4">
                <img src="assets/images/frown-solid.svg" alt="check" class="img-fluid">
              </div>
            </div>
            <h3 class="m-2 text-center">Lo sentimos</h3>
            <p class="text-center text-muted mb-1">Por el momento no tenemos cupos disponibles.</p>

            <div class="d-flex justify-content-center mb-3 mt-4">
              <div class="col-4">
                <a class="btn btn-primary btn-block" href="/">Reintentar</a>
              </a>
            </div>
          </div>
        </div>
      </div>
    </main>
  </body>
</html>
