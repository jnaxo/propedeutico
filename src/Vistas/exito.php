<!doctype html>
<html lang="en">
  <head>
    <?php require_once '../src/Vistas/parciales/cabecera.php' ?>

    <title>Inscripción Propedeutico</title>
    <link rel="stylesheet" type="text/css" href="assets/css/app.css">
  </head>
  <body>
    <main class="container">

      <div class="row mt-5 justify-content-md-center">
        <div class="col-sm-12 col-md-8 col-xl-6">
          <h1 class="text-center mb-5">Inscripción Propedeutico</h1>

          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-success btn-circle" disabled="disabled" id="btn-1">1</button>
                <p>Datos personales</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-success btn-circle" disabled="disabled" id="btn-2">2</button>
                <p>Talleres</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-success btn-circle" disabled="disabled" id="btn-3">3</button>
                <p>Confirmar</p>
              </div>
            </div>
          </div>

          <div class="shadow-lg p-3 mb-3 mt-3 bg-white" id="step-3">
            <div class="d-flex justify-content-center m-4">
              <div class="col-4">
                <img src="assets/images/check-solid.svg" alt="check" class="img-fluid">
              </div>
            </div>
            <h3 class="m-2 text-center">GRACIAS POR TU INSCRIPCIÓN</h3>
            <p class="text-center text-muted mb-3">Hemos registrado correctamente tu solicitud.<p>
            <div class="d-flex justify-content-center mb-3 mt-4">
              <div class="col-4">
                <a class="btn btn-secondary btn-block" href="/">Volver</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </body>
</html>
