<!doctype html>
<html lang="en">
  <head>
    <?php require_once '../src/Vistas/parciales/cabecera.php' ?>

    <title>Inscripción Propedeutico</title>
    <link rel="stylesheet" type="text/css" href="assets/css/app.css">
  </head>
  <body>
    <main class="container">

      <div class="row mt-5 justify-content-md-center">
        <div class="col-sm-12 col-md-8 col-xl-6">
          <h1 class="text-center mb-5">Inscripción Propedeutico</h1>

          <div class="stepwizard">
            <div class="stepwizard-row">
              <div class="stepwizard-step">
                <button type="button" class="btn btn-primary btn-circle" disabled="disabled" id="btn-1">1</button>
                <p>Datos personales</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-secondary btn-circle" disabled="disabled" id="btn-2">2</button>
                <p>Talleres</p>
              </div>
              <div class="stepwizard-step">
                <button type="button" class="btn btn-secondary btn-circle" disabled="disabled" id="btn-3">3</button>
                <p>Confirmar</p>
              </div>
            </div>
          </div>

          <form method="post">
            <!-- Step 1 -->
            <div class="shadow-lg p-3 mb-3 mt-3 bg-white" id="step-1">
              <h4 class="m-2 text-center">Datos personales</h4>
              <div class="form-group">
                <input
                  type="text"
                  class="form-control"
                  placeholder="RUT"
                  id="rut"
                  name="rut"
                  required>
                <div class="invalid-feedback">Debes ingresar un RUT válido</div>
              </div>
              <div class="form-group">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Nombre"
                  id="name"
                  name="name"
                  required>
                <div class="invalid-feedback">Debes ingresar un nombre válido</div>
              </div>
              <div class="form-group">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Apellido paterno"
                  id="firstSurname"
                  name="first_surname"
                  required>
                <div class="invalid-feedback">Debes ingresar un Apellido paterno válido</div>
              </div>
              <div class="form-group">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Apellido materno"
                  id="secondSurname"
                  name="second_surname"
                  required>
                <div class="invalid-feedback">Debes ingresar un Apellido materno válido</div>
              </div>
              <div class="form-group">
                <input
                  type="tel"
                  class="form-control"
                  placeholder="Teléfono"
                  id="phone"
                  name="phone"
                  required>
                <div class="invalid-feedback">El número de teléfono debe tener el formato +569 99999999</div>
              </div>
              <div class="form-group">
                <input
                  type="email"
                  class="form-control"
                  placeholder="Correo electrónico"
                  id="email"
                  name="email"
                  required>
                <div class="invalid-feedback">Debes ingresar un email válido</div>
              </div>
              <div class="form-group">
                <select class="form-control" id="school" name="school" required>
                  <option value="">Seleccione colegio</option>
                </select>
                <div class="invalid-feedback">Debes seleccionar un colegio</div>
              </div>
              <div class="form-group">
                <select class="form-control" id="course" name="course" required>
                  <option value="">Seleccione año cursado</option>
                  <option>1º Medio</option>
                  <option>2º Medio</option>
                  <option>3º Medio</option>
                  <option>4º Medio</option>
                </select>
                <div class="invalid-feedback">Debes seleccionar el año que estás cursando</div>
              </div>

              <!-- Nav -->
              <div class="d-flex justify-content-center">
                <div class="col-4">
                  <button type="button" class="btn btn-secondary btn-block back-btn" disabled>Volver</button>
                </div>
                <div class="col-4">
                  <button type="button" class="btn btn-primary btn-block next-btn">Continuar</button>
                </div>
              </div>

            </div>

            <!-- Step 2 -->
            <div class="shadow-lg p-3 mb-3 mt-3 bg-white hidden" id="step-2">
              <h4 class="m-2 text-center">Talleres disponibles</h4>

              <div class="form-group">
                <select class="form-control" id="workshop">
                  <option value="">Seleccione un taller</option>
                </select>
                <div class="invalid-feedback">Debes seleccionar un taller</div>
              </div>

              <div class="form-group">
                <select class="form-control" id="workshopGroup" disabled>
                  <option value="">Seleccione un grupo</option>
                </select>
                <div class="invalid-feedback">Debes seleccionar un grupo</div>
              </div>

              <input type="hidden" name="groups" id="groupsInput">

              <div id="workshops-preview" class="hidden">
                <h4 class="m-2 text-center">Talleres inscritos</h4>
                <table class="table table-borderless" id="workshop-table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Taller</th>
                      <th scope="col">Horario</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>
                        <button type="button" class="btn btn-outline-danger btn-sm">X</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- Nav -->
              <div class="d-flex justify-content-center">
                <div class="col-4">
                  <button type="button" class="btn btn-secondary btn-block back-btn" disabled>Volver</button>
                </div>
                <div class="col-4">
                  <button type="button" class="btn btn-primary btn-block next-btn">Continuar</button>
                </div>
              </div>
            </div>

            <!-- Step 3 -->
            <div class="shadow-lg p-3 mb-3 mt-3 bg-white hidden" id="step-3">
              <h4 class="m-2 text-center">Confirma tu inscripción</h4>

              <h5 class="m-2">Datos personales</h5>
              <table class="table table-borderless">
                <tr><th>Nombre</th><td id="cname">Foo bar sas</td></tr>
                <tr><th>RUT</th><td id="crut">17.128.123-1</td></tr>
                <tr><th>Teléfono</th><td id="cphone">120122192</td></tr>
                <tr><th>Email</th><td id="cemail">foo@bar.com</td></tr>
                <tr><th>Colegio</th><td id="cschool">Lorem ipsum</td></tr>
                <tr><th>Curso</th><td id="ccourse">4º Medio</td></tr>
              </table>

              <h5 class="m-2">Talleres inscritos</h5>
              <table class="table table-borderless" id="display-workshop-table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Taller</th>
                    <th scope="col">Horario</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                  </tr>
                </tbody>
              </table>

              <!-- Nav -->
              <div class="d-flex justify-content-center">
                <div class="col-4">
                  <button type="button" class="btn btn-secondary btn-block back-btn" disabled>Volver</button>
                </div>
                <div class="col-4">
                  <button type="submit" class="btn btn-success btn-block next-btn" id="">Confirmar</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </main>

    <?php require_once '../src/Vistas/parciales/scripts.php' ?>
  </body>
</html>
