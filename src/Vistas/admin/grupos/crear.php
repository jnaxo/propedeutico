<!doctype html>
<html lang="es">
  <head>
    <?php require_once '../../../src/Vistas/parciales/cabecera.php' ?>

    <title>Nuevo Grupo - Admin Propedeutico</title>
  </head>
  <body>
    <?php require_once '../../../src/Vistas/parciales/navbar.php' ?>

    <main class="container mt-4">
      <h1 class="h3 mb-0">Crear Grupo</h1>

      <a href="/admin/talleres" class="btn btn-info mt-3 mb-3">Volver</a>
      <div class="row d-flex justify-content-center">
        <div class="col-8">
          <div class="card shadow h-100 mt-4 p-0">
            <div class="card-body">

            <form method="POST" class="container pl-5 pr-5 mt-4">
              <div class="form-group row">
                <label for="taller" class="col-md-3 col-form-label">Taller</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="taller" value="<?= $taller->getNombre() ?>" disabled>
                </div>
              </div>
              <div class="form-group row">
                <label for="dia" class="col-md-3 col-form-label">Día</label>
                <div class="col-md-9">
                  <select id="dia" class="form-control" required name="dia">
                    <option selected value="">Elegir...</option>
                    <option value="Lu">Lunes</option>
                    <option value="Ma">Martes</option>
                    <option value="Mi">Miércoles</option>
                    <option value="Ju">Jueves</option>
                    <option value="Vi">Viernes</option>
                    <option value="Sa">Sábado</option>
                    <option value="Do">Domingo</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="bloque" class="col-md-3 col-form-label">Bloque</label>
                <div class="col-md-9">
                  <select id="bloque" class="form-control" name="bloque" required>
                    <option selected value="">Elegir...</option>
                    <option>[1-2] 8:30 - 10:00</option>
                    <option>[3-4] 10:15 - 11:45</option>
                    <option>[5-6] 12:00 - 13:30</option>
                    <option>[7-8] 14:30 - 16:00</option>
                    <option>[9-10] 16:15 - 17:45</option>
                    <option>[11-12] 18:00 - 19:30</option>
                    <option>[13-14] 19:45 - 21:15</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="cupos" class="col-md-3 col-form-label">Cupos</label>
                <div class="col-md-4">
                  <input
                    type="number"
                    class="form-control"
                    id="cupos"
                    name="cupos"
                    min="1"
                    max="999"
                    placeholder="Ingrese cupos"
                    required>
                </div>
              </div>

              <div class="form-group row justify-content-md-center mb-0">
                <div class="col-md-5 col-sm-10 col-lg-3 mt-3">
                  <button type="submit" class="btn btn-primary btn-block">Guardar</button>
                </div>
              </div>
            </form>

            </div>
          </div>

        </div>
      </div>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
