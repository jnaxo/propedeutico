<!doctype html>
<html lang="es">
  <head>
    <?php require_once '../../../src/Vistas/parciales/cabecera.php' ?>

    <title>Talleres - Admin Propedeutico</title>
  </head>
  <body>
    <?php require_once '../../../src/Vistas/parciales/navbar.php' ?>

    <main class="container mt-4">
      <div class="row justify-content-between">
        <h1 class="h3 mb-0">Talleres</h1>
        <a href="talleres/crear.php" class="btn btn-success">Nuevo Taller</a>
      </div>

        <?php foreach ($talleres as $taller) : ?>
          <div class="card text-center mb-5 shadow h-100 mt-4 p-0">
            <div class="card-body">
              <h3 class="card-title text-capitalize"><?= $taller->getNombre() ?></h3>
              <p class="text-muted"><?= $taller->getDescripcion() ?></p>
              <p class="card-text">
                Inscritos <span class="badge badge-dark">
                  <?= $taller->getCuposDisponibles() ?> / <?= $taller->getCuposTotales() ?>
                </span>
                <br>
                Estado
                <?php if (true) : ?>
                  <span class="badge badge-primary">Activo</span>
                <?php else : ?>
                  <span class="badge badge-secondary">Inactivo</span>
                <?php endif ?>
              </p>
            </div>

            <?php if (count($taller->getGrupos())) : ?>
              <div class="mt-2 p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Grupo</th>
                      <th scope="col">Horario</th>
                      <th scope="col">Cupos</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($taller->getGrupos() as $grupo) : ?>
                      <tr>
                        <td><?= $grupo->getBloque() ?></td>
                        <td><?= $grupo->getHorario() ?></td>
                        <td><?= $grupo->getCupos() ?> </td>
                        <td>
                          <a href="talleres/borrar-grupo.php?grupo=<?= $grupo->getId() ?>"
                            class="btn btn-sm btn-outline-danger">
                            Eliminar
                          </a>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            <?php else : ?>
              <p class="text-muted mb-4" >No hay grupos de horario registrados</p>
            <?php endif ?>
            <div class="card-footer">
              <div class="row justify-content-end">
                <div class="col-3">
                  <a href="talleres/crear-grupo.php?taller=<?= $taller->getId() ?>" class="btn btn-outline-primary">
                    Agregar Grupo
                  </a>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>

    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
