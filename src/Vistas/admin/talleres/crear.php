<!doctype html>
<html lang="es">
  <head>
    <?php require_once '../../../src/Vistas/parciales/cabecera.php' ?>

    <title>Nuevo Taller - Admin Propedeutico</title>
  </head>
  <body>
    <?php require_once '../../../src/Vistas/parciales/navbar.php' ?>

    <main class="container mt-4">
      <h1 class="h3 mb-0">Crear Taller</h1>

      <a href="/admin/talleres" class="btn btn-info mt-3 mb-3">Volver</a>

      <div class="row d-flex justify-content-center">
        <div class="col-8">
          <div class="card shadow h-100 mt-4 p-0">
            <div class="card-body">

            <form method="POST" class="container pl-5 pr-5 mt-4">
              <div class="form-group row">
                <label for="nombre" class="col-md-3 col-form-label">Nombre</label>
                <div class="col-md-9">
                  <input
                    type="text"
                    class="form-control"
                    id="nombre"
                    name="nombre"
                    placeholder="Ingrese nombre"
                    required>
                </div>
              </div>
              <div class="form-group row">
                <label for="descripcion" class="col-md-3 col-form-label">Descripción</label>
                <div class="col-md-9">
                  <input type="text"
                    class="form-control"
                    id="descripcion"
                    name="descripcion"
                    placeholder="Ingrese descripción"
                    required>
                </div>
              </div>

              <div class="form-group row justify-content-md-center mb-0">
                <div class="col-md-5 col-sm-10 col-lg-3 mt-3">
                  <button type="submit" class="btn btn-success btn-block">Crear</button>
                </div>
              </div>
            </form>

            </div>
          </div>

        </div>
      </div>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
