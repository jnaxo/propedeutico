<!doctype html>
<html lang="es">
  <head>
    <?php require_once '../../src/Vistas/parciales/cabecera.php' ?>

    <title>Dashboard - Admin Propedeutico</title>
  </head>
  <body>
    <?php require_once '../../src/Vistas/parciales/navbar.php' ?>

    <main class="container mt-4">
      <h1 class="h3 mb-0">Dashboard</h1>

      <div class="row justify-content-around mt-4">
        <div class="col">
          <div class="card shadow h-100 py-2">
            <div class="card-body">
              <h5 class="text-primary">Talleres</h5>
              <h2 class="text-secondary"><span class="count"><?= count($talleres) ?></span></h2>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow h-100 py-2">
            <div class="card-body">
              <h5 class="text-success">Inscripciones</h5>
              <h2 class="text-secondary"><span class="count"><?= $inscritos ?></span></h2>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow h-100 py-2">
            <div class="card-body">
              <h5 class="text-info">Cupos</h5>
              <h2 class="text-secondary"><span class="count"><?= $cupos ?></span></h2>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow h-100 py-2">
            <div class="card-body">
              <h5 class="text-warning">Colegios</h5>
              <h2 class="text-secondary"><span class="count"><?= $colegios ?></span></h2>
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-around mt-4">
        <div class="col-8">
          <div class="card shadow h-100 py-2" style="min-height: 400px">
            <div class="card-body">
              <canvas id="inscritosCurso"></canvas>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card shadow h-100 py-2">
            <div class="card-body">
              <canvas id="alumnosColegios"></canvas>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"></script>
    <script>
        var talleres = <?= json_encode($talleres->toArray()) ?>;
        var colegios = <?= json_encode($inscritosColegio->toArray()) ?>;
        var cursosTaller = <?= json_encode($cursosTaller->toArray()) ?>;
    </script>
    <script src="/assets/js/admin.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  </body>
</html>
