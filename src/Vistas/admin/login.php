<!doctype html>
<html lang="en">
  <head>
    <?php require_once '../../src/Vistas/parciales/cabecera.php' ?>

    <title>Admin Propedeutico</title>
  </head>
  <body>
    <main class="container">
      <div class="row justify-content-md-center">
        <div class="col-5">
          <div class="card mt-5">
            <div class="card-body">
              <h3 class="text-muted text-center">Acceso Administrador</h3>
              <h1 class="card-title text-center">Iniciar sesión</h1>
              <form method="POST" class="m-5">
                <div class="form-group">
                  <label for="email">Correo electrónico</label>
                  <input type="email" class="form-control
                    <?= $error ? 'is-invalid' : '' ?>"
                    name="email" id="email" placeholder="Ingrese su correo" required>
                </div>
                <div class="form-group">
                  <label for="password">Contraseña</label>
                  <input type="password" class="form-control <?= $error ? 'is-invalid' : '' ?>"
                    name="password" id="password" placeholder="Ingrese su contraseña" required>
                    <div class="invalid-feedback">
                      Credenciales inválidas
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Iniciar sesión</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </main>
    <?php require_once '../../src/Vistas/parciales/scripts.php' ?>
  </body>
</html>
