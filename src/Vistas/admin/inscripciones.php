<!doctype html>
<html lang="es">
  <head>
    <?php require_once '../../src/Vistas/parciales/cabecera.php' ?>

    <title>Inscripciones - Admin Propedeutico</title>
  </head>
  <body>
    <?php require_once '../../src/Vistas/parciales/navbar.php' ?>

    <main class="container mt-4">
      <h1 class="h3 mb-0">Inscripciones</h1>

      <div class="mt-4 card shadow h-100 p-0">
      <?php if (count($inscritos) > 0) : ?>
        <div class="table-responsive">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Taller</th>
              <th scope="col">Grupo</th>
              <th scope="col">RUT</th>
              <th scope="col">Nombre Completo</th>
              <th scope="col">Teléfono</th>
              <th scope="col">Correo Electrónico</th>
              <th scope="col">Colegio</th>
              <th scope="col">Curso</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($inscritos as $inscrito) : ?>
              <tr>
                <th scope="row"><?= $inscrito->getId() ?></th>
                <td><?= $inscrito->getGrupo()->getTaller()->getNombre() ?></td>
                <td><?= $inscrito->getGrupo()->getBloque() ?></td>
                <td><?= $inscrito->getUsuario()->getRut() ?></td>
                <td><?= $inscrito->getUsuario()->getNombreCompleto() ?></td>
                <td><?= $inscrito->getUsuario()->getTelefono() ?></td>
                <td><?= $inscrito->getUsuario()->getEmail() ?></td>
                <td><?= $inscrito->getUsuario()->getColegio()->getNombre() ?></td>
                <td><?= $inscrito->getUsuario()->getCurso() ?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
        </div>
      <?php else : ?>
        <p class="text-center text-muted pt-5 pb-4"> No hay registros</p>
      <?php endif ?>
      </div>
    </main>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
