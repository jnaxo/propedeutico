<nav class="navbar navbar-dark bg-primary navbar-expand-lg">
  <a class="navbar-brand" href="/admin">Propedeutico</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav d-flex align-items-center w-100">
      <li class="nav-item">
        <a class="nav-link" href="/admin">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/inscripciones.php">Inscripciones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/talleres">Talleres</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/usuarios">Usuarios</a>
      </li>

      <li class="nav-item dropdown active ml-auto">
        <?php if (array_key_exists('admin_id', $_SESSION)) { ?>
          <a
            class="nav-link dropdown-toggle pr-5"
            href=""
            id="navbarDropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-user fa-user-circle fa-lg mr-2"></i>
            <span class="header-username"><?= $_SESSION['admin_nombre'] ?></span>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <form action="/admin/logout.php" method="POST">
                <button type="submit" class="dropdown-item">Cerrar sesión</button>
            </form>
          </div>
        <?php } ?>
      </li>
    </ul>
  </div>
</nav>
