<?php
namespace Controladores;

require_once '../../../bin/config.php';

use Modelos\AdminQuery;
use Modelos\Admin;

class Usuario
{
    public function __construct()
    {
        session_start();

        if (!array_key_exists('admin_id', $_SESSION)) {
            header("Location: /admin/login.php");
            die();
        }
    }

    public function mostrar()
    {
        $usuarios = AdminQuery::create()->find();
        include('../../../src/Vistas/admin/usuarios/index.php');
    }

    public function crear()
    {
        include('../../../src/Vistas/admin/usuarios/crear.php');
    }

    public function guardar($request)
    {
        $existe = AdminQuery::create()
            ->filterByEmail($request['email'])
            ->findOne();
        if ($existe) {
            //
        } else {
            $admin = new Admin();
            $admin->setNombre($request['nombre']);
            $admin->setApellidos($request['apellidos']);
            $admin->setEmail($request['email']);
            $admin->setPassword($request['password']);
            $admin->setTipo($request['tipo']);
            $admin->save();
        }

        header("Location: /admin/usuarios");
        die();
    }
}
