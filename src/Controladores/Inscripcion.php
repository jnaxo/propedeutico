<?php
namespace Controladores;

require_once '../bin/config.php';

use Modelos\TallerQuery;
use Modelos\ColegioQuery;
use Modelos\Usuario;
use Modelos\UsuarioQuery;
use Modelos\GrupoQuery;
use Modelos\Inscripcion as InscripcionModelo;

class Inscripcion
{
    public function __construct()
    {
        session_start();
    }

    public function index()
    {
        $talleres = TallerQuery::create()
            ->joinWith('Grupo')
            ->where('Grupo.cupos > ?', 0)
            ->find()
            ->toArray();

        if (count($talleres) < 1) {
            include('../src/Vistas/sincupos.php');
            return;
        }

        $colegios = ColegioQuery::create()->find()->toArray();
        include('../src/Vistas/inscripcion.php');
    }

    public function inscribir($request)
    {
        unset($_SESSION['error']);
        $rut = $request['rut'];
        $email = $request['email'];
        $gruposIds = json_decode($request['groups']);
        $duplicado = UsuarioQuery::create()->filterByRut($rut)
            ->useInscripcionQuery()
                ->filterByGrupoId($gruposIds)
                ->useGrupoQuery()
                    ->useTallerQuery()
                    ->endUse()
                ->endUse()
            ->endUse()
            ->select(['Taller.Nombre'])->find()->toArray();

        if ($duplicado) {
            $talleres = count($duplicado) < 3 ? implode(' y ', $duplicado) : implode(', ', $duplicado);
            $_SESSION['error'] = 'Tu RUT ya se encuentra registrado en <br>' . $talleres;
        } else {
            $usuarioExiste = UsuarioQuery::create()->findByRut($rut)->getFirst();
            $usuario = $usuarioExiste ? $usuarioExiste : new Usuario();
            $usuario->setNombre($request['name']);
            $usuario->setRut($rut);
            $usuario->setApellidoPat($request['first_surname']);
            $usuario->setApellidoMat($request['second_surname']);
            $usuario->setTelefono($request['phone']);
            $usuario->setEmail($email);
            $usuario->setCurso($request['course']);
            $usuario->setColegioId($request['school']);
            $usuario->save();

            foreach ($gruposIds as $grupoId) {
                $inscripcion = new InscripcionModelo();
                $inscripcion->setGrupoId($grupoId);
                $inscripcion->setUsuarioId($usuario->getId());
                $inscripcion->setComentarios('');
                $inscripcion->save();

                $grupo = GrupoQuery::create()->findPk($grupoId);
                $grupo->setCupos($grupo->getCupos() - 1);
                $grupo->save();
            }
            $_SESSION['email'] = $email;
        }

        header('Location: resultado.php');
        die();
    }

    public function mostrarExito()
    {
        include('../src/Vistas/exito.php');
    }

    public function mostrarError($mensaje)
    {
        include('../src/Vistas/error.php');
    }

    public function indexAdmin()
    {
        $inscritos = InscripcionQuery::create()->find();
        include('../../../src/Vistas/admin/inscripciones.php');
    }
}
