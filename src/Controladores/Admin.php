<?php

namespace Controladores;

require_once '../../bin/config.php';

use Modelos\InscripcionQuery;
use Modelos\TallerQuery;
use Modelos\GrupoQuery;
use Modelos\UsuarioQuery;
use Modelos\ColegioQuery;

class Admin
{
    public function __construct()
    {
        session_start();

        if (!array_key_exists('admin_id', $_SESSION)) {
            header("Location: /admin/login.php");
            die();
        }
    }

    public function mostrarDashboard()
    {
        $talleres = TallerQuery::create()
            ->select(['Nombre'])
            ->find();
        $inscritos = InscripcionQuery::create()
            ->select(['inscritos'])
            ->withColumn('COUNT(*)', 'inscritos')
            ->findOne();
        $cupos = GrupoQuery::create()
            ->select(['total'])
            ->withColumn('SUM(cupos)', 'total')
            ->groupByTallerId()
            ->findOne();
        $colegios = InscripcionQuery::create()
            ->select(['total'])
            ->joinWithUsuario()
            ->withColumn('SUM(colegio_id)', 'total')
            ->groupBy('Usuario.colegio_id')
            ->findOne();
        $inscritosColegio = ColegioQuery::create()
            ->select(['Colegio.Nombre', 'cantidad'])
            ->joinWithUsuario()
            ->withColumn('COUNT(colegio_id)', 'cantidad')
            ->groupBy('Usuario.colegio_id')
            ->find();
        $cursosTaller = UsuarioQuery::create()
            ->select(['Taller.Nombre', 'Usuario.Curso', 'cantidad'])
            ->join('Inscripcion')
            ->join('Inscripcion.Grupo')
            ->join('Grupo.Taller')
            ->withColumn('COUNT(grupos.taller_id)', 'cantidad')
            ->groupBy('Grupo.TallerId')
            ->groupBy('Usuario.Curso')
            ->find();

        include('../../src/Vistas/admin/dashboard.php');
    }

    public function mostrarInscripciones()
    {
        $inscritos = InscripcionQuery::create()
            ->joinWithUsuario()
            ->joinWithGrupo()
            ->joinWith('Grupo.Taller')
            ->find();
        include('../../src/Vistas/admin/inscripciones.php');
    }
}
