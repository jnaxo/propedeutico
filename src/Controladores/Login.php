<?php

namespace Controladores;

require_once '../../bin/config.php';

use Modelos\AdminQuery;

class Login
{
    public function __construct()
    {
        session_start();
    }

    public function mostrarLogin($error = null)
    {
        include('../../src/Vistas/admin/login.php');
    }

    public function authenticar($request)
    {
        $admin = AdminQuery::create()
            ->filterByEmail($request['email'])
            ->findOne();
        if ($admin) {
            if ($admin->getPassword() === $request['password']) {
                $_SESSION['admin_id'] = $admin->getId();
                $_SESSION['admin_nombre'] = $admin->getNombre() . ' '
                    . substr($admin->getApellidos(), 0, 1) . '.';
                header("Location: /admin/index.php");
                die();
            }
        } else {
            $_SESSION['error'] = 'Credenciales inválidas';
            header("Location: /admin/login.php");
            die();
        }
    }
}
