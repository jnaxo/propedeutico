<?php
namespace Controladores;

require_once '../../../bin/config.php';

use Modelos\Taller as TallerModelo;
use Modelos\TallerQuery;
use Modelos\Grupo;
use Modelos\GrupoQuery;

class Taller
{
    public function __construct()
    {
        session_start();

        if (!array_key_exists('admin_id', $_SESSION)) {
            header("Location: /admin/login.php");
            die();
        }
    }

    public function mostrar()
    {
        $talleres = TallerQuery::create()
            ->leftJoinWith('Grupo')
            ->find();
        include('../../../src/Vistas/admin/talleres/index.php');
    }

    public function crear()
    {
        include('../../../src/Vistas/admin/talleres/crear.php');
    }

    public function guardar($request)
    {
        $taller = new TallerModelo();
        $taller->setNombre($request['nombre']);
        $taller->setDescripcion($request['descripcion']);
        $taller->setActivo(1);
        $taller->save();

        header("Location: /admin/talleres");
        die();
    }

    public function crearGrupo($tallerId)
    {
        $taller = TallerQuery::create()->findPk($tallerId);
        include('../../../src/Vistas/admin/grupos/crear.php');
    }

    public function guardarGrupo($taller_id, $request)
    {
        $bloque = explode(']', substr($request['bloque'], 1));
        $grupo = new Grupo();
        $grupo->setTallerId($taller_id);
        $grupo->setCupos($request['cupos']);
        $grupo->setBloque($bloque[0]);
        $grupo->setHorario($request['dia'] . ' ' . $bloque[1]);
        $grupo->save();

        header("Location: /admin/talleres");
        die();
    }

    public function quitarGrupo($grupo_id)
    {
        $grupo = GrupoQuery::create()->findPK($grupo_id);
        $grupo->delete();

        header("Location: /admin/talleres");
        die();
    }
}
