<?php

namespace Modelos\Base;

use \Exception;
use \PDO;
use Modelos\Grupo as ChildGrupo;
use Modelos\GrupoQuery as ChildGrupoQuery;
use Modelos\Map\GrupoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'grupos' table.
 *
 *
 *
 * @method     ChildGrupoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGrupoQuery orderByTallerId($order = Criteria::ASC) Order by the taller_id column
 * @method     ChildGrupoQuery orderByCupos($order = Criteria::ASC) Order by the cupos column
 * @method     ChildGrupoQuery orderByBloque($order = Criteria::ASC) Order by the bloque column
 * @method     ChildGrupoQuery orderByHorario($order = Criteria::ASC) Order by the horario column
 * @method     ChildGrupoQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildGrupoQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildGrupoQuery groupById() Group by the id column
 * @method     ChildGrupoQuery groupByTallerId() Group by the taller_id column
 * @method     ChildGrupoQuery groupByCupos() Group by the cupos column
 * @method     ChildGrupoQuery groupByBloque() Group by the bloque column
 * @method     ChildGrupoQuery groupByHorario() Group by the horario column
 * @method     ChildGrupoQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildGrupoQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildGrupoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGrupoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGrupoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGrupoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGrupoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGrupoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGrupoQuery leftJoinTaller($relationAlias = null) Adds a LEFT JOIN clause to the query using the Taller relation
 * @method     ChildGrupoQuery rightJoinTaller($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Taller relation
 * @method     ChildGrupoQuery innerJoinTaller($relationAlias = null) Adds a INNER JOIN clause to the query using the Taller relation
 *
 * @method     ChildGrupoQuery joinWithTaller($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Taller relation
 *
 * @method     ChildGrupoQuery leftJoinWithTaller() Adds a LEFT JOIN clause and with to the query using the Taller relation
 * @method     ChildGrupoQuery rightJoinWithTaller() Adds a RIGHT JOIN clause and with to the query using the Taller relation
 * @method     ChildGrupoQuery innerJoinWithTaller() Adds a INNER JOIN clause and with to the query using the Taller relation
 *
 * @method     ChildGrupoQuery leftJoinInscripcion($relationAlias = null) Adds a LEFT JOIN clause to the query using the Inscripcion relation
 * @method     ChildGrupoQuery rightJoinInscripcion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Inscripcion relation
 * @method     ChildGrupoQuery innerJoinInscripcion($relationAlias = null) Adds a INNER JOIN clause to the query using the Inscripcion relation
 *
 * @method     ChildGrupoQuery joinWithInscripcion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Inscripcion relation
 *
 * @method     ChildGrupoQuery leftJoinWithInscripcion() Adds a LEFT JOIN clause and with to the query using the Inscripcion relation
 * @method     ChildGrupoQuery rightJoinWithInscripcion() Adds a RIGHT JOIN clause and with to the query using the Inscripcion relation
 * @method     ChildGrupoQuery innerJoinWithInscripcion() Adds a INNER JOIN clause and with to the query using the Inscripcion relation
 *
 * @method     \Modelos\TallerQuery|\Modelos\InscripcionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildGrupo findOne(ConnectionInterface $con = null) Return the first ChildGrupo matching the query
 * @method     ChildGrupo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGrupo matching the query, or a new ChildGrupo object populated from the query conditions when no match is found
 *
 * @method     ChildGrupo findOneById(int $id) Return the first ChildGrupo filtered by the id column
 * @method     ChildGrupo findOneByTallerId(int $taller_id) Return the first ChildGrupo filtered by the taller_id column
 * @method     ChildGrupo findOneByCupos(int $cupos) Return the first ChildGrupo filtered by the cupos column
 * @method     ChildGrupo findOneByBloque(string $bloque) Return the first ChildGrupo filtered by the bloque column
 * @method     ChildGrupo findOneByHorario(string $horario) Return the first ChildGrupo filtered by the horario column
 * @method     ChildGrupo findOneByCreatedAt(string $created_at) Return the first ChildGrupo filtered by the created_at column
 * @method     ChildGrupo findOneByUpdatedAt(string $updated_at) Return the first ChildGrupo filtered by the updated_at column *

 * @method     ChildGrupo requirePk($key, ConnectionInterface $con = null) Return the ChildGrupo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOne(ConnectionInterface $con = null) Return the first ChildGrupo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGrupo requireOneById(int $id) Return the first ChildGrupo filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByTallerId(int $taller_id) Return the first ChildGrupo filtered by the taller_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByCupos(int $cupos) Return the first ChildGrupo filtered by the cupos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByBloque(string $bloque) Return the first ChildGrupo filtered by the bloque column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByHorario(string $horario) Return the first ChildGrupo filtered by the horario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByCreatedAt(string $created_at) Return the first ChildGrupo filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGrupo requireOneByUpdatedAt(string $updated_at) Return the first ChildGrupo filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGrupo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGrupo objects based on current ModelCriteria
 * @method     ChildGrupo[]|ObjectCollection findById(int $id) Return ChildGrupo objects filtered by the id column
 * @method     ChildGrupo[]|ObjectCollection findByTallerId(int $taller_id) Return ChildGrupo objects filtered by the taller_id column
 * @method     ChildGrupo[]|ObjectCollection findByCupos(int $cupos) Return ChildGrupo objects filtered by the cupos column
 * @method     ChildGrupo[]|ObjectCollection findByBloque(string $bloque) Return ChildGrupo objects filtered by the bloque column
 * @method     ChildGrupo[]|ObjectCollection findByHorario(string $horario) Return ChildGrupo objects filtered by the horario column
 * @method     ChildGrupo[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildGrupo objects filtered by the created_at column
 * @method     ChildGrupo[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildGrupo objects filtered by the updated_at column
 * @method     ChildGrupo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GrupoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Modelos\Base\GrupoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Modelos\\Grupo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGrupoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGrupoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGrupoQuery) {
            return $criteria;
        }
        $query = new ChildGrupoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGrupo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GrupoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GrupoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGrupo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, taller_id, cupos, bloque, horario, created_at, updated_at FROM grupos WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGrupo $obj */
            $obj = new ChildGrupo();
            $obj->hydrate($row);
            GrupoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGrupo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GrupoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GrupoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GrupoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GrupoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the taller_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTallerId(1234); // WHERE taller_id = 1234
     * $query->filterByTallerId(array(12, 34)); // WHERE taller_id IN (12, 34)
     * $query->filterByTallerId(array('min' => 12)); // WHERE taller_id > 12
     * </code>
     *
     * @see       filterByTaller()
     *
     * @param     mixed $tallerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByTallerId($tallerId = null, $comparison = null)
    {
        if (is_array($tallerId)) {
            $useMinMax = false;
            if (isset($tallerId['min'])) {
                $this->addUsingAlias(GrupoTableMap::COL_TALLER_ID, $tallerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tallerId['max'])) {
                $this->addUsingAlias(GrupoTableMap::COL_TALLER_ID, $tallerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_TALLER_ID, $tallerId, $comparison);
    }

    /**
     * Filter the query on the cupos column
     *
     * Example usage:
     * <code>
     * $query->filterByCupos(1234); // WHERE cupos = 1234
     * $query->filterByCupos(array(12, 34)); // WHERE cupos IN (12, 34)
     * $query->filterByCupos(array('min' => 12)); // WHERE cupos > 12
     * </code>
     *
     * @param     mixed $cupos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByCupos($cupos = null, $comparison = null)
    {
        if (is_array($cupos)) {
            $useMinMax = false;
            if (isset($cupos['min'])) {
                $this->addUsingAlias(GrupoTableMap::COL_CUPOS, $cupos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cupos['max'])) {
                $this->addUsingAlias(GrupoTableMap::COL_CUPOS, $cupos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_CUPOS, $cupos, $comparison);
    }

    /**
     * Filter the query on the bloque column
     *
     * Example usage:
     * <code>
     * $query->filterByBloque('fooValue');   // WHERE bloque = 'fooValue'
     * $query->filterByBloque('%fooValue%', Criteria::LIKE); // WHERE bloque LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bloque The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByBloque($bloque = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bloque)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_BLOQUE, $bloque, $comparison);
    }

    /**
     * Filter the query on the horario column
     *
     * Example usage:
     * <code>
     * $query->filterByHorario('fooValue');   // WHERE horario = 'fooValue'
     * $query->filterByHorario('%fooValue%', Criteria::LIKE); // WHERE horario LIKE '%fooValue%'
     * </code>
     *
     * @param     string $horario The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByHorario($horario = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($horario)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_HORARIO, $horario, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(GrupoTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(GrupoTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(GrupoTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(GrupoTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GrupoTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Modelos\Taller object
     *
     * @param \Modelos\Taller|ObjectCollection $taller The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByTaller($taller, $comparison = null)
    {
        if ($taller instanceof \Modelos\Taller) {
            return $this
                ->addUsingAlias(GrupoTableMap::COL_TALLER_ID, $taller->getId(), $comparison);
        } elseif ($taller instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(GrupoTableMap::COL_TALLER_ID, $taller->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTaller() only accepts arguments of type \Modelos\Taller or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Taller relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function joinTaller($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Taller');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Taller');
        }

        return $this;
    }

    /**
     * Use the Taller relation Taller object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Modelos\TallerQuery A secondary query class using the current class as primary query
     */
    public function useTallerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTaller($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Taller', '\Modelos\TallerQuery');
    }

    /**
     * Filter the query by a related \Modelos\Inscripcion object
     *
     * @param \Modelos\Inscripcion|ObjectCollection $inscripcion the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGrupoQuery The current query, for fluid interface
     */
    public function filterByInscripcion($inscripcion, $comparison = null)
    {
        if ($inscripcion instanceof \Modelos\Inscripcion) {
            return $this
                ->addUsingAlias(GrupoTableMap::COL_ID, $inscripcion->getGrupoId(), $comparison);
        } elseif ($inscripcion instanceof ObjectCollection) {
            return $this
                ->useInscripcionQuery()
                ->filterByPrimaryKeys($inscripcion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInscripcion() only accepts arguments of type \Modelos\Inscripcion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Inscripcion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function joinInscripcion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Inscripcion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Inscripcion');
        }

        return $this;
    }

    /**
     * Use the Inscripcion relation Inscripcion object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Modelos\InscripcionQuery A secondary query class using the current class as primary query
     */
    public function useInscripcionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInscripcion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Inscripcion', '\Modelos\InscripcionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGrupo $grupo Object to remove from the list of results
     *
     * @return $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function prune($grupo = null)
    {
        if ($grupo) {
            $this->addUsingAlias(GrupoTableMap::COL_ID, $grupo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the grupos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GrupoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GrupoTableMap::clearInstancePool();
            GrupoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GrupoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GrupoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GrupoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GrupoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(GrupoTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(GrupoTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(GrupoTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(GrupoTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(GrupoTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildGrupoQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(GrupoTableMap::COL_CREATED_AT);
    }

} // GrupoQuery
