<?php

namespace Modelos;

use Modelos\Base\Admin as BaseAdmin;

/**
 * Skeleton subclass for representing a row from the 'admins' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Admin extends BaseAdmin
{
    public function getTipoAdmin()
    {
        $tipo = $this->getTipo();
        switch ($tipo) {
            case 1:
                return 'Administrador';
            case 2:
                return 'Coordinador';
            
            default:
                return 'Básico';
        }
        return '';
    }
}
