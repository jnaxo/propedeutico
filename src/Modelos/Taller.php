<?php

namespace Modelos;

use Modelos\Base\Taller as BaseTaller;
use Modelos\GrupoQuery;
use Modelos\InscripcionQuery;

/**
 * Skeleton subclass for representing a row from the 'talleres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Taller extends BaseTaller
{
    public function getCuposDisponibles()
    {
        $inscritos = InscripcionQuery::create()
            ->select(['inscritos'])
            ->joinWithGrupo()
            ->where('Grupo.taller_id = ?', $this->getId())
            ->withColumn('COUNT(*)', 'inscritos')
            ->findOne();
        return $inscritos ? $inscritos : 0;
    }

    public function getCuposTotales()
    {
        $cupos = GrupoQuery::create()
            ->select(['total'])
            ->filterByTallerId($this->getId())
            ->withColumn('SUM(cupos)', 'total')
            ->groupByTallerId()
            ->findOne();
        return $cupos ? $cupos : 0;
    }
}
