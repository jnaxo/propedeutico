# Inscrioción Propedeutico
Proyecto PHP con Propel ORM para inscripción propedeutico

## Requerimientos
- Mysql
- Composer
- PHP 7.1 o superior

## Instalación:
- Crear base de datos MySql con nombre `propedeutico`
- Ejecutar en terminal `composer install`
- Ejecutar en terminal `vendor/bin/propel diff`
- Ejecutar en terminal `vendor/bin/propel migrate`
